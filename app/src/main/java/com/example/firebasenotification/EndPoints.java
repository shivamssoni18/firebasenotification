package com.example.firebasenotification;

public class EndPoints {
    public static final String URL_REGISTER_DEVICE = "http://10.1.81.213/FcmExample/RegisterDevice.php";
    public static final String URL_SEND_SINGLE_PUSH = "http://10.1.81.213/FcmExample/sendSinglePush.php";
    public static final String URL_SEND_MULTIPLE_PUSH = "http://10.1.81.213/FcmExample/sendMultiplePush.php";
    public static final String URL_FETCH_DEVICES = "http://10.1.81.213/FcmExample/GetRegisteredDevices.php";
}